var path = require('path');
var fs = require('fs');
var express = require('express');
var React = require('react');
var ReactDOM = require('react-dom');
var Q = require('q');

// Server part
var app = express();
app.use('/', express.static(path.resolve(__dirname, path.join('src/client'))));

var server = app.listen(1337);
console.log('Server listening on port 1337');

// Socket.IO part
var io = require('socket.io')(server);

var userList = [];
var allActs = [];
var actIndex = -1;

io.on('connection', function (socket) {
    console.log('New client connected!');
 
    socket.on('fetchAct', function() {
        if (!allActs || allActs.length === 0) {
            fs.readFile('_acts.json', 'utf8', function(err, acts) {
                allActs = JSON.parse(acts);
            });
        }
        if (actIndex < 0 || allActs.length <= actIndex) {
            return;
        }
        io.emit('act', JSON.stringify(allActs[actIndex]));
    });

    socket.on('nextAct', function () {
        if (parseInt(actIndex, 10) === allActs.length - 1) {
            return;
        }
        actIndex = actIndex + 1;
    });

    socket.on('fetchScores', function () {
        fs.readFile('_scores.json', 'utf8', function(err,scores) {
            scores = JSON.parse(scores || '[]');
            if (!scores || scores.length === 0) {
                return;
            }
            io.emit('scores', scores.filter(s => s.actIndex === actIndex));
        });
    });

    socket.on('newScore', function (rating) {
        fs.readFile('_scores.json', 'utf8', function(err, scores) {
            scores = JSON.parse(scores || '[]');
            rating = JSON.parse(rating);
            scores.push({
                userId: rating.userId,
                username: rating.username,
                score: rating.score,
                comment: rating.comment,
                actIndex: actIndex
            });
			fs.writeFile('_scores.json', JSON.stringify(scores, null, 4), function (err) {
				io.emit('scores', scores.filter(s => s.actIndex === actIndex));
			});
		});
    });

    socket.on('fetchRankings', function () {
        fs.readFile('_scores.json', 'utf8', function(err,scores) {
            scores = JSON.parse(scores || '[]');
            if (!scores || scores.length === 0) {
                return;
            }
            
            var rankingObjs = [];
            for (var i = 0; i <= actIndex; i ++) {
                var rankFilters = scores.filter(s => s.actIndex === i);
                if (rankFilters && rankFilters.length > 0) {
                    rankingObjs.push({
                        country: allActs[i].country,
                        score: rankFilters.map(f => f.score).reduce((a, b) => parseInt(a, 10) + parseInt(b, 10))
                    });
                }
            }

            rankingObjs = rankingObjs.sort((a, b) => { return b.score - a.score; });
            io.emit('rankings', rankingObjs);
        });
    })

    socket.on('userLogin', function (user) {
        if (userList.length === 0 || !userList.some(u => u.id === user.id)) {
            userList.push(user);
        }
        io.emit('updateUsers', userList);
    });
});

app.get('/', function (req, res) {
    res.sendFile('index.html', {root: path.join(__dirname, 'src/client')});
});