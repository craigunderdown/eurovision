import * as React from 'react';
import { render } from 'react-dom';

class MainPage extends React.Component {
    constructor() {
        super();
        var user = JSON.parse(localStorage['EV_2017']);
        this.state = {
			scores: [],
            thisUser: user,
            users: [],
            currentAct: null,
            rankings: []
		};
    }

	componentDidMount() {
		var that = this;
		this.socket = io();
		this.socket.on('scores', function (scores) {
			that.setState({ scores: scores });
		});
        this.socket.on('updateUsers', function(users) {
            that.setState({ users: users });
        });
        this.socket.on('act', function(act) {
            that.setState({
                currentAct: JSON.parse(act)
            });
        });
        this.socket.on('rankings', function(rankings) {
            that.setState({ rankings: rankings });
        })
        this.socket.emit('fetchScores');
        this.socket.emit('fetchAct');
        this.socket.emit('fetchRankings');
        this.socket.emit('userLogin', this.props.user);
	}
	
    submitScore() {
		var scoreNum = this.refs['panel-score'].value;
        var scoreComment = this.refs['panel-comment'].value || '';
        var scoreSub = {
            userId: this.state.thisUser.id,
            score: scoreNum,
            comment: scoreComment,
            username: this.state.thisUser.name
        };
        this.socket.emit('newScore', JSON.stringify(scoreSub), function (err) {
			if (err) return console.error('New score error:', err);
		});
	}

    canScore() {
        return !!this.state.currentAct 
                && !this.state.scores.find(s => s.userId === this.state.thisUser.id)
                && this.state.thisUser.name !== "server-user";
    }

    moveToNextAct() {
        this.socket.emit('nextAct');
        this.socket.emit('fetchAct');
        this.socket.emit('fetchScores');
        this.socket.emit('fetchRankings');
    }

	render() {
        var userListElems = [];
        if (this.state.users && this.state.users.length > 0) {
            userListElems = this.state.users.filter(u => u.name !== 'server-user').map(u => {
                var isMe = u.id === this.state.thisUser.id;
                return (
                    <div className="user-item" key={`user-${u.id}`}>
                        <div className="user-icon"></div>
                        <div className="user-name">{`${u.name} ${isMe ? '(you)' : ''}`}</div>
                    </div>
                );
            });
        }

        var countryRankListElems = [];
        if (this.state.rankings && this.state.rankings.length > 0) {
            countryRankListElems = this.state.rankings.map((r, idx) => {
                return (
                    <div className="rank-item" key={`rank-country-${r.country}-${idx}`}>
                        <div className="rank-pos">{idx + 1}</div>
                        <div className="rank-name">{r.country}</div>
                        <div className="rank-score">{r.score}</div>
                    </div>
                );
            })
        }

        var userScoreElems = [];
        if (this.state.scores && this.state.scores.length > 0) {
            userScoreElems = this.state.scores.map(s => {
                var scorePerson = this.state.users.find(u => u.id === s.userId);
                return (
                    <div className="score-item" key={`score-${s.userId}-${s.actId}`}>
                        <div className="user-score-num">{s.score}</div>
                        <div className="user-score-name">{scorePerson ? scorePerson.name : 'Logged Off User...'}</div>
                        <div className="user-comment">{s.comment || 'No Comment...'}</div>
                    </div>
                );
            });
        }
        
		return (
			<div className="score-page">
                <div className="ranking-list">
                    <h3>Current Ranking</h3>
                    {countryRankListElems} 
                </div>
                <div className="score-content">
                    <div className="score-title">
                        <img src='http://www.underconsideration.com/brandnew/archives/eurovision_logo_detail.png' />
                        <span>
                            {!!this.state.thisUser.isAdmin
                            ? (
                                <button type="button" onClick={this.moveToNextAct.bind(this)}>NEXT ACT</button>
                            ):null}
                        </span> 
                    </div>
                    <div className="next-act">
                        {this.state.currentAct 
                        ? (
                            <div className="act-group">
                                <div className="act-flag">
                                    <img src={this.state.currentAct.flagUrl} />
                                </div>
                                <div className="act-country">{this.state.currentAct.country}</div>
                                <div className="act-name">{this.state.currentAct.name}</div>
                            </div>
                        ) : (
                            <div className="act-group">
                                <div className="act-flag"></div>
                                <div className="act-country"><i>Awaiting First Act...</i></div>
                                <div className="act-name"></div>
                            </div>
                        )}
                    </div>
                    <div className="user-scores">
                        <h2>Scores</h2>
                        {userScoreElems}
                    </div>
                    {this.canScore()
                    ? (
                        <form className="score-panel">
                            <div className="panel-inputs">
                                <select ref="panel-score">
                                    <option value="0">0</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    <option value="10">10</option>
                                </select>
                                <input type="text" ref="panel-comment" />
                            </div>
                            <button type="button" onClick={this.submitScore.bind(this)}>Submit</button>
                        </form>
                    ):null}
                </div>
                <div className="user-list">
                    <h3>Connected Users</h3>
                    {userListElems} 
                </div>
			</div>
		);
	}
}

export default MainPage;