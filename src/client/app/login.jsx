import * as React from 'react';
import { render } from 'react-dom';
import MainPage from './mainpage.jsx';

var LoginPage = React.createClass({
    getInitialState: function () {
        if (!localStorage || !localStorage['EV_2017']) {
            return {
                isLoggedIn: false,
                loggedInUser: null
            };
        } else {
            var user = JSON.parse(localStorage['EV_2017']);
            return {
                isLoggedIn: true,
                loggedInUser: user
            };
        }
    },
    userProvidedLogin: function() {
        var name = this.refs['login-name'].value;
        if (!name) {
            return;
        }
        if (!localStorage) { 
            return console.error("Ruh roh, this shouldn't be happening...");
        }
        
        var user = {
            id: Math.ceil(Math.random() * Math.pow(10, 6)).toString() + '_EV2017',
            name: name
        };

        // Future Craig - This is past Craig. Please update this horrible method of determining the host server.
        if (user.name === 'server-user') {
            user.isAdmin = true;
        }
        
        // Use localStorage to sign the user in. Use sessions if/when improving this.
        localStorage['EV_2017'] = JSON.stringify(user);

        // I'm sure there's a better way than just reloading the page... But I'm too tired to think of it.
        location.reload();
    },
    render: function () {
        // Simple functionality. It'll do for local use only.
        if (!!this.state.isLoggedIn) {
            return (
                <MainPage user={this.state.loggedInUser} />
            );
        }
        return (
            <form onSubmit={this.userProvidedLogin} className="login-form">
                <span>Please Enter Your Name</span>
                <input ref="login-name" type="text" />
            </form>
        );
    }
});

render(
	<LoginPage />,
	document.getElementById('app')
);